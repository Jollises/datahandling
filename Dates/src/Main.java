import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Main {

    public static void main(String[] args) {

        Main main = new Main();
        System.out.println(main.convertDate("28 February 2015"));
        System.out.println(main.convertDate("22 December 2016"));
    }

    //Exercise 2.3.
    //Дана строка вида "28 Февраль 2015" или “28 February 2015” (*"28 февраля 2015"). Необходимо сконвертировать ее в
    // вид “28/фев/15” или "28/Feb/15". Локаль учитывать не нужно (* нужно). Использовать SimpleDateFormat
    // или DateTimeFormatter
    private String convertDate(String string) {

        String convertedDate;
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yy", Locale.ENGLISH);
        try {
            Date date = formatter.parse(string);
            formatter.applyPattern("dd/MMM/yy");
            convertedDate = formatter.format(date);
        } catch (ParseException e) {
            convertedDate = "Unable to parse";
            e.printStackTrace();
        }
        return convertedDate;
    }
}
