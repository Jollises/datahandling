package com.jollises;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

        Main main = new Main();

        String str = "Some, input:   text?!";
        System.out.println(main.stringWithWordsOnly(str));

        System.out.println(main.shortLongWord(main.stringWithWordsOnly(str)));

        String strWithPhoneNum = "Some text +7 (3412) 517-647 8 (3412) 4997-12 +7 3412 90-41-90 text " +
                "in the end";
        System.out.println(main.deletePrefix(strWithPhoneNum));

        String templateKey = "Уважаемый, $userName, извещаем вас о том, что на вашем счете $accountNumber скопилась сумма, " +
                "превышающая стоимость $monthNumber месяцев пользования нашими услугами. Деньги продолжают поступать. " +
                "Вероятно, вы неправильно настроили автоплатеж. С уважением, $userName $userPost";
        String templateValue = "Николай 123 12 Менеджер";
        System.out.println(main.strFromTemplate(templateKey, templateValue));
    }


    //Exercise 1.1.
    // Дана строка, содержащая "обычный" текст, т.е. слова, знаки препинания, переносы строк и т.п., например,
    // "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?".
    //Необходимо получить строку, в которой содержатся только слова из исходной строки,
    // разделенные знаком переноса строки, в нижнем регистре
    private String stringWithWordsOnly(String string) {

        Pattern pattern = Pattern.compile("[\\w']+");
        Matcher matcher = pattern.matcher(string);

        StringBuilder stringBuilder = new StringBuilder("");

        while (matcher.find()) {

            stringBuilder.append(string.substring(matcher.start(), matcher.end()).toLowerCase());
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

    //Exercise 1.2.
    //В предыдущей задаче найти самое короткое слово, самое длинное слово
    private String shortLongWord(String str) {

        String[] words = str.split("\\s+");
        String minWord = "";
        String maxWord = "";

        int max = words[0].length();

        for (String word : words) {
            if (word.length() > max) {
                maxWord = word;
            } else {
                minWord = word;
            }

        }
        return "The shortest word: " + minWord + "\n" + "The longest word: " + maxWord;
    }

    //Exercise 1.3.
    //Дана строка, содержащая в себе, помимо прочего, номера телефонов. Необходимо удалить из этой строки префиксы
    // локальных номеров, соответствующих Ижевску. Например, из "+7 (3412) 517-647" получить "517-647";
    // "8 (3412) 4997-12" > "4997-12"; "+7 3412 90-41-90" > "90-41-90"
    private String deletePrefix(String string) {

        return string.replaceAll("(8|\\+7)\\s(\\(3412\\)|(3412))", "");
    }

    //Exercise 1.4.
    //Дана строка-шаблон, содержащая заготовку письма. Например,
    //"Уважаемый, $userName, извещаем вас о том, что на вашем счете $номерСчета скопилась сумма, превышающая стоимость
    // $числоМесяцев месяцев пользования нашими услугами. Деньги продолжают поступать. Вероятно, вы неправильно
    // настроили автоплатеж. С уважением, $пользовательФИО $должностьПользователя"
    //Также дана одна пара строк. templateKey и templateValue. Необходимо в строке заменить все
    // placeholders (строки $имяШаблона, т.е. '$' + templateKey) на значения из templateValue
    private String strFromTemplate(String templateKey, String templateValue) {

        String[] templateValues = templateValue.split(" ");
        Pattern pattern = Pattern.compile("\\$\\w+");
        Matcher matcher = pattern.matcher(templateKey);

        StringBuffer stringBuffer = new StringBuffer("");

        while (matcher.find()) {
            switch (templateKey.substring(matcher.start(), matcher.end())) {
                case "$userName":
                    matcher.appendReplacement(stringBuffer, templateValues[0]);
                    break;
                case "$accountNumber":
                    matcher.appendReplacement(stringBuffer, templateValues[1]);
                    break;
                case "$monthNumber":
                    matcher.appendReplacement(stringBuffer, templateValues[2]);
                    break;
                case "$userPost":
                    matcher.appendReplacement(stringBuffer, templateValues[3]);
                    break;
            }
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }
}
