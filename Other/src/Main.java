public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        System.out.println(main.isThirdNumSum("0.15, 0.1, 0.25"));
        System.out.println(main.isThirdNumSum("0.1,0.1,0.2"));
        System.out.println(main.isThirdNumSum("2 3 5"));
        System.out.println(main.isThirdNumSum("2 3 2"));
    }

    //Exercise 3.2.
    //Даны три числа, например, 0.1, 0.15 и 0.25. Числа даны в виде строки. Необходимо ответить, является ли третье
    // число суммой двух первых.
    private String isThirdNumSum(String string) {
        String answer;
        String[] numbers = string.split("((,\\s)|(,)|\\s)");
        double sumOfNum = Double.parseDouble(numbers[0]) + Double.parseDouble(numbers[1]);
        if (sumOfNum == Double.parseDouble(numbers[2])) {
            answer = "That's true";
        } else {
            answer = "They are not equals";
        }
        return answer;
    }
}
